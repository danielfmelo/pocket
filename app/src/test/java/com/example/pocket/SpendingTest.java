package com.example.pocket;

import com.example.pocket.model.Spending;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class SpendingTest {
    @Test
    public void spending_notNull(){
        Spending spending = new Spending();
        assertNotNull(spending);
    }

    @Test
    public void spending_null(){
        Spending spending = null;
        assertNull(spending);
    }

    @Test
    public void spending_listEquals(){
        Spending[] spendings = new Spending[10];
        Spending[] spendings2 = new Spending[10];

        spendings[0] = new Spending();
        spendings2[0] = new Spending();

        Assert.assertArrayEquals(spendings, spendings);
    }

    @Test
    public void spending_instance(){
        Spending spending = new Spending();
        spending.setId(1);
        spending.setPrice(40);
        spending.setDescription("TV");
        spending.setCategory("Food");

        assertThat(spending, instanceOf(Spending.class));
        assertNotNull(spending);
    }
}
