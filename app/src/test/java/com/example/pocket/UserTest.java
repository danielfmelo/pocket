package com.example.pocket;

import com.example.pocket.model.User;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class UserTest {
    @Test
    public void user_notNull(){
        User user = new User();
        assertNotNull(user);
    }

    @Test
    public void user_null(){
        User user = null;
        assertNull(user);
    }

    @Test
    public void user_isEquals(){
        User user1 = new User();

        user1.setId(1);
        user1.setSalary(9000);
        user1.setPass("1234");
        user1.setUsername("daniel");

        assertEquals(user1, user1);
    }

    @Test
    public void user_instance(){
        User user1 = new User();

        user1.setId(1);
        user1.setSalary(9000);
        user1.setPass("1234");
        user1.setUsername("daniel");

        assertThat(user1, instanceOf(User.class));
    }
}
