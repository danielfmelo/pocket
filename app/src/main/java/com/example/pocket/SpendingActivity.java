package com.example.pocket;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pocket.adapter.SpendingAdapter;
import com.example.pocket.model.Spending;
import com.example.pocket.model.User;
import com.example.pocket.threads.CategoryThread;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SpendingActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    ListView listSpending;
    TextView total;
    User user;
    ArrayList<Spending> spendings;

    SpendingAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spending);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button

        listSpending = findViewById(R.id.list_spending);
        user = (User)getIntent().getSerializableExtra("user");
        spendings = (ArrayList<Spending>) getIntent().getSerializableExtra("spending");

        float total = 0;

        for (Spending s:
             spendings) {
            total += s.getPrice();
        }

        TextView totalText = findViewById(R.id.total);
        TextView leftText = findViewById(R.id.left);

        totalText.setText("$" + user.getSalary() + " - $" + total);
        leftText.setText("$" + (user.getSalary() - total));

        adapter = new SpendingAdapter(this, spendings);
        listSpending.setAdapter(adapter);
        listSpending.setOnItemClickListener(this);
        listSpending.setOnItemLongClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Spending s = spendings.get(position);

        Toast.makeText(getApplicationContext(), s.getDate_purchase(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Spending s = spendings.get(position);
        User user = (User)getIntent().getSerializableExtra("user");
        ExecutorService myService = Executors.newFixedThreadPool(3);
        CategoryThread thread = new CategoryThread(user, this, "EDIT", s);
        myService.execute(thread);
        myService.shutdown();
        return false;
    }
}
