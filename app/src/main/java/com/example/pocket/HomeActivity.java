package com.example.pocket;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pocket.model.User;
import com.example.pocket.threads.CategoryThread;
import com.example.pocket.threads.SpendingThread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void navigateNewSpending(View v){
        User user = (User)getIntent().getSerializableExtra("user");
        ExecutorService myService = Executors.newFixedThreadPool(3);
        CategoryThread thread = new CategoryThread(user, this, "NEW", null);
        myService.execute(thread);
        myService.shutdown();
    }

    public void navigateListSpending(View v){
        User user = (User)getIntent().getSerializableExtra("user");
        ExecutorService myService = Executors.newFixedThreadPool(3);
        SpendingThread thread = new SpendingThread(user, this);
        myService.execute(thread);
        myService.shutdown();
    }
}
