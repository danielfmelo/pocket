package com.example.pocket;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pocket.http.SpendingHttp;
import com.example.pocket.model.Category;
import com.example.pocket.model.User;

import java.util.ArrayList;

public class NewSpendingActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView listCategories;
    ArrayList<Category> categories;
    int selectedCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_spending);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button

        listCategories = findViewById(R.id.list_categories);

        String[] ctg = new String[10];

        categories = (ArrayList<Category>) getIntent().getSerializableExtra("categories");

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, categories);
        listCategories.setAdapter(adapter);

        listCategories.setOnItemClickListener(this);
    }

    public boolean validateInformation(EditText price, EditText description){
        if(selectedCategory == 0){
            Toast.makeText(this, "Select category", Toast.LENGTH_SHORT).show();
            return false;
        }else if(price.getText().toString().isEmpty()){
            Toast.makeText(this, "Inform price", Toast.LENGTH_SHORT).show();
            return false;
        }else if(description.getText().toString().isEmpty()){
            Toast.makeText(this, "Inform description", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void addSpending(View v) {
        String content = "";
        User user = (User) getIntent().getSerializableExtra("user");

        EditText editPrice = (EditText) findViewById(R.id.new_price);
        EditText editDescription = (EditText) findViewById(R.id.new_description);


        if(!validateInformation(editPrice, editDescription)) return;
        float price = Float.parseFloat(editPrice.getText().toString());
        String description = editDescription.getText().toString();

        SpendingHttp spendingHttp = new SpendingHttp(getApplicationContext(), this);
        spendingHttp.add(user, selectedCategory, price, description);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String category = parent.getItemAtPosition(position).toString();
        selectedCategory = Integer.parseInt(category.substring(0,1));
        Toast.makeText(this, category + " selected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
