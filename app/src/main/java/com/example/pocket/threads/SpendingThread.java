package com.example.pocket.threads;


import android.content.Context;
import android.content.Intent;

import com.example.pocket.HomeActivity;
import com.example.pocket.SpendingActivity;
import com.example.pocket.model.Spending;
import com.example.pocket.model.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class SpendingThread implements Runnable {
    private User user;
    private Context context;
    public SpendingThread(User user, Context context){
        this.user = user;
        this.context = context;
    }

    public ArrayList<Spending> getSpendings(String uri) {

        String content="";
        ArrayList<Spending> spendings = new ArrayList();

        try {
            URL url = new URL(uri);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestProperty("Content-Type", "application/json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            content = stringBuilder.toString();

            System.out.println(content);

            spendings = Spending.JSONTListoObject(content);

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return spendings;
    }

    public void run() {
        String uri = "http://pocket.oficinanamao.com.br/api/spending/list.php?user="+user.getId();
        ArrayList<Spending> spds = this.getSpendings(uri);

        if(!spds.isEmpty()){
            Intent i = new Intent(context, SpendingActivity.class);
            i.putExtra("user", (Serializable) user);
            i.putExtra("spending", (Serializable) spds);
            context.startActivity(i);
        }else{
            Intent i = new Intent(context, HomeActivity.class);
            i.putExtra("user", (Serializable) user);
            context.startActivity(i);
        }
    }
}

