package com.example.pocket.threads;


import android.content.Context;
import android.content.Intent;

import com.example.pocket.HomeActivity;
import com.example.pocket.model.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

public class NewSpendingThread implements Runnable {
    private User user;
    private int category;
    private String description;
    private float price;
    private Context context;

    public NewSpendingThread(User user, int category, String description, float price, Context context) {
        this.user = user;
        this.category = category;
        this.description = description;
        this.price = price;
        this.context = context;
    }

    public boolean addSpending(String uri) {

        String content="";

        try {
            URL url = new URL(uri);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestProperty("Content-Type", "application/json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            content = stringBuilder.toString();
            System.out.println(content);

            if(content.equals("true")){
                return true;
            }
            return false;

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return false;
    }

    public void run() {
        String uri = "http://pocket.oficinanamao.com.br/api/spending/add.php?user="+user.getId()+"&category="+category+"&price="+price+"&description="+description;
        boolean res = this.addSpending(uri);

        if(res){
            Intent i = new Intent(context, HomeActivity.class);
            i.putExtra("user", (Serializable) user);
            context.startActivity(i);
        }
    }
}

