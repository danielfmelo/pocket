package com.example.pocket.threads;


import android.content.Context;
import android.content.Intent;

import com.example.pocket.HomeActivity;
import com.example.pocket.MainActivity;
import com.example.pocket.model.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

public class RegisterThread implements Runnable {
    private String username;
    private String password;
    private float salary;
    private Context context;
    public RegisterThread(String username, String password, float salary, Context context){
        this.password = password;
        this.username = username;
        this.salary = salary;
        this.context = context;
    }

    public User register(String uri) {

        String content="";
        User user = null;

        try {
            URL url = new URL(uri);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestProperty("Content-Type", "application/json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            content = stringBuilder.toString();

            System.out.println(content);

            user = User.JSONToUser(content);

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return user;
    }

    public void run() {
        System.out.println("Initiating Request: " + username);

        String uri = "http://pocket.oficinanamao.com.br/api/user/add.php?username="+username+"&pass="+password+"&salary="+salary;
        User user = this.register(uri);

        if(user != null){
            Intent i = new Intent(context, HomeActivity.class);
            i.putExtra("user", (Serializable) user);
            context.startActivity(i);
        }else{
            Intent i = new Intent(context, MainActivity.class);
            context.startActivity(i);
        }
    }
}

