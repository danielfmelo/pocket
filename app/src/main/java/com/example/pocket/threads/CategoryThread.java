package com.example.pocket.threads;


import android.content.Context;
import android.content.Intent;

import com.example.pocket.EditSpendingActivity;
import com.example.pocket.NewSpendingActivity;
import com.example.pocket.model.Category;
import com.example.pocket.model.Spending;
import com.example.pocket.model.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class CategoryThread implements Runnable {
    private User user;
    private String page;
    private Context context;
    Spending spending;
    public CategoryThread(User user, Context context, String page, Spending spending){
        this.user = user;
        this.context = context;
        this.page = page;
        this.spending = spending;
    }

    public ArrayList<Category> getCategories(String uri) {

        String content="";
        ArrayList<Category> categories = new ArrayList<>();
        try {
            URL url = new URL(uri);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestProperty("Content-Type", "application/json");

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            content = stringBuilder.toString();

            System.out.println(content);

            categories = Category.JSONToListCategory(content);

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return categories;
    }

    public void run() {
        String uri = "http://pocket.oficinanamao.com.br/api/category/list.php";
        ArrayList<Category> categories = this.getCategories(uri);

        if(categories != null){
            if(page.equals("NEW")){
                Intent i = new Intent(context, NewSpendingActivity.class);
                i.putExtra("user", (Serializable) user);
                i.putExtra("categories", (Serializable) categories);
                context.startActivity(i);
            }else if(page.equals("EDIT")){
                Intent i = new Intent(context, EditSpendingActivity.class);
                i.putExtra("user", (Serializable) user);
                i.putExtra("spending", (Serializable) spending);
                i.putExtra("categories", (Serializable) categories);
                context.startActivity(i);
            }
        }
    }
}

