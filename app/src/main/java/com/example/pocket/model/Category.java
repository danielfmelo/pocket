package com.example.pocket.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {
    private int id;
    private String category;
    private int sts;

    public Category(int id, String category, int sts) {
        this.id = id;
        this.category = category;
        this.sts = sts;
    }

    public Category(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getSts() {
        return sts;
    }

    public void setSts(int sts) {
        this.sts = sts;
    }


    @Override
    public String toString() {
        return id + " - " + category;
    }

    public static ArrayList<Category> JSONToListCategory(String s) {

        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Category>  categories = null;

        try {
            categories = mapper.readValue(s, new TypeReference<List<Category>>(){});
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return categories;
    }
}
