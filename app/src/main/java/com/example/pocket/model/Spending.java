package com.example.pocket.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Spending implements Serializable {
    private int id;
    private float price;
    private String date_purchase;
    private String description;
    private String category;
    private int sts;

    public Spending(int id, float price, String date_purchase, String description, String category, int sts) {
        this.id = id;
        this.price = price;
        this.date_purchase = date_purchase;
        this.description = description;
        this.category = category;
        this.sts = sts;
    }

    public Spending() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDate_purchase() {
        return date_purchase;
    }

    public void setDate_purchase(String date_purchase) {
        this.date_purchase = date_purchase;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getSts() {
        return sts;
    }

    public void setSts(int sts) {
        this.sts = sts;
    }

    public static ArrayList<Spending> JSONTListoObject(String s) {

        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Spending> spendings = null;

        try {
            spendings = mapper.readValue(s, new TypeReference<List<Spending>>(){});
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }


        return spendings;
    }
}
