package com.example.pocket.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.pocket.R;
import com.example.pocket.model.Spending;

import java.util.ArrayList;
import java.util.List;


public class SpendingAdapter extends ArrayAdapter<Spending> {

    private Context mContext;
    private List<Spending> moviesList = new ArrayList<>();

    public SpendingAdapter( Context context, ArrayList<Spending> list) {
        super(context, 0 , list);
        mContext = context;
        moviesList = list;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_item,parent,false);

        Spending currentMovie = moviesList.get(position);

        TextView description = (TextView)listItem.findViewById(R.id.textView_poster);
        description.setText(currentMovie.getDescription());

        TextView name = (TextView) listItem.findViewById(R.id.textView_name);
        name.setText("$" + String.valueOf(currentMovie.getPrice()));

        TextView categoty = (TextView) listItem.findViewById(R.id.textView_release);
        categoty.setText(currentMovie.getCategory());

        return listItem;
    }
}