package com.example.pocket;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pocket.http.UserHttp;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public boolean validateInformation(EditText username, EditText pass){
        if(username.getText().toString().isEmpty()){
            Toast.makeText(this, "Inform username", Toast.LENGTH_SHORT).show();
            return false;
        }else if(pass.getText().toString().isEmpty()){
            Toast.makeText(this, "Inform password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void onBtnClick(View v){

        EditText username = findViewById(R.id.username);
        EditText pass = findViewById(R.id.password);

        if(!validateInformation(username, pass)) return;

        UserHttp userHttp = new UserHttp(getApplicationContext(), this);

        userHttp.login(username.getText().toString(), pass.getText().toString());
    }

    public void onRegisterClick(View v){
        Intent i = new Intent(this, RegisterActivity.class);
        startActivity(i);


    }


}
