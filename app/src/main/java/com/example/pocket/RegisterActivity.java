package com.example.pocket;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pocket.threads.RegisterThread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public boolean validateInformation(EditText username, EditText pass, EditText salary){
        if(username.getText().toString().isEmpty()){
            Toast.makeText(this, "Inform username", Toast.LENGTH_SHORT).show();
            return false;
        }else if(pass.getText().toString().isEmpty()){
            Toast.makeText(this, "Inform password", Toast.LENGTH_SHORT).show();
            return false;
        }else if(salary.getText().toString().isEmpty()){
            Toast.makeText(this, "Inform monthly income", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void registerUser(View v){
        EditText username = findViewById(R.id.register_username);
        EditText pass = findViewById(R.id.register_password);
        EditText salary = findViewById(R.id.register_salary);

        if(!validateInformation(username, pass, salary)) return;

        ExecutorService myService = Executors.newFixedThreadPool(3);

        RegisterThread ds1 = new RegisterThread(
                username.getText().toString(),
                pass.getText().toString(),
                Float.parseFloat(salary.getText().toString()),
                this);

        myService.execute(ds1);

        myService.shutdown();
    }
}
