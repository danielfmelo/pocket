package com.example.pocket.http;

import okhttp3.OkHttpClient;

public class HttpClient {
    static OkHttpClient client;

    public static OkHttpClient createInstance() {
        if(client == null) {
            client = new OkHttpClient();
        }
        return client;
    }
}
