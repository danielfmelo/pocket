package com.example.pocket.http;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.pocket.HomeActivity;
import com.example.pocket.model.User;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Serializable;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class UserHttp {
    OkHttpClient client;
    Context context;
    Activity v;

    public UserHttp (Context context, Activity v){
        client = HttpClient.createInstance();
        this.context = context;
        this.v = v;

    }

    public void login(String username, String pass){
        String uri = "http://pocket.oficinanamao.com.br/api/user/login.php?username="+username+"&pass="+pass;
        Request request = new Request.Builder().url(uri).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                v.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "Request failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if(response.isSuccessful()){
                    String json = response.body().string();
                    User user = User.JSONToUser(json);


                    if(user != null){
                        Intent i = new Intent(context, HomeActivity.class);
                        i.putExtra("user", (Serializable) user);
                        v.startActivity(i);
                    }else{
                        v.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context, "Invalid User ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });
    }
}
