package com.example.pocket.http;

import android.app.Activity;
import android.content.Context;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.pocket.R;
import com.example.pocket.adapter.SpendingAdapter;
import com.example.pocket.model.Spending;
import com.example.pocket.model.User;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SpendingHttp {
    OkHttpClient client;
    Context context;
    Activity v;

    public SpendingHttp(Context context, Activity v){
        client = HttpClient.createInstance();
        this.context = context;
        this.v = v;

    }

    public void add(User user, int category, float price, String description){
        String uri = "http://pocket.oficinanamao.com.br/api/spending/add.php?user="+user.getId()+"&category="+category+"&price="+price+"&description="+description;
        Request request = new Request.Builder().url(uri).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                v.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "Request failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if(response.isSuccessful()){
                    String content = response.body().string();

                    System.out.println(content);

                    if(content.equals("true")){
                        v.runOnUiThread(new Runnable() {
                            public void run() {
                                EditText desc = v.findViewById(R.id.new_description);
                                EditText price = v.findViewById(R.id.new_price);

                                desc.setText("");
                                price.setText("");
                                Toast.makeText(context, "Spending added", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        v.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context, "Error adding spending", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });
    }

    public void edit(Spending spending, int category, float price, String description){
        String uri = "http://pocket.oficinanamao.com.br/api/spending/edit.php?id="+spending.getId()+"&category="+category+"&price="+price+"&description="+description;
        Request request = new Request.Builder().url(uri).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                v.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "Request failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if(response.isSuccessful()){
                    String content = response.body().string();

                    System.out.println(content);

                    if(content.equals("true")){
                        v.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context, "Spending edited", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        v.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context, "Error editing spending", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });
    }

    public void list(User user){
        String uri = "http://pocket.oficinanamao.com.br/api/spending/list.php?user="+user.getId();
        Request request = new Request.Builder().url(uri).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                v.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "Request failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if(response.isSuccessful()){
                    String content = response.body().string();

                    System.out.println(content);
                    final ArrayList<Spending> spendings = Spending.JSONTListoObject(content);

                    v.runOnUiThread(new Runnable() {
                        public void run() {
                            ListView listSpending = v.findViewById(R.id.list_spending);
                            SpendingAdapter adapter = new SpendingAdapter(context, spendings);
                            listSpending.setAdapter(adapter);
                        }
                    });

                }
            }
        });
    }
}
